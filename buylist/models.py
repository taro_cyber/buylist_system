from accounts.models import CustomUser
from django.db import models
# Create your models here.

class Goods(models.Model):
    id = models.ForeignKey(CustomUser, verbose_name="ユーザー", on_delete=models.PROTECT, primary_key=True)
    # good_id = models.AutoField()
    good_name = models.CharField(verbose_name="品名", max_length=50)
    good_price = models.IntegerField(verbose_name="価格")
    good_deadline = models.DateField(verbose_name="購入予定日", auto_now=False, auto_now_add=False)
    good_purvhase_data = models.DateField(verbose_name="購入日", auto_now=False, auto_now_add=False)
    good_status = models.BooleanField()
    good_create_data = models.DateTimeField(verbose_name="作成日時", auto_now=True, auto_now_add=False)
    good_purohace_shop = models.TextField(verbose_name='購入店舗', null=True)
    good_update_data = models.DateTimeField(verbose_name='更新日時',auto_now_add=True)
    good_prionty = models.CharField(verbose_name='重要度',max_length=1)
   
    def __str__(self):
        return self.good_name